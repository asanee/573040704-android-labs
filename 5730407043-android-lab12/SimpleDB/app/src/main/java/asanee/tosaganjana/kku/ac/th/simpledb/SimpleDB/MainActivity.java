package asanee.tosaganjana.kku.ac.th.simpledb.SimpleDB;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import asanee.tosaganjana.kku.ac.th.simpledb.R;
import asanee.tosaganjana.kku.ac.th.simpledb.SimpleDB.DBHandler;
import asanee.tosaganjana.kku.ac.th.simpledb.tasks.TasksActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edtId, edtName;
    TextView tvText;
    Button btnInsert, btnDelete, btnView, btnUpdate;
    DBHandler db;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.simpleDB:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;
            case R.id.tasks:
                Intent intent2 = new Intent(this, TasksActivity.class);
                startActivity(intent2);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtId = (EditText) findViewById(R.id.id);
        edtName = (EditText) findViewById(R.id.name);
        tvText = (TextView) findViewById(R.id.text);
        btnDelete = (Button) findViewById(R.id.delete);
        btnInsert = (Button) findViewById(R.id.insert);
        btnUpdate = (Button) findViewById(R.id.update);
        btnView = (Button) findViewById(R.id.view);

        btnDelete.setOnClickListener(this);
        btnView.setOnClickListener(this);
        btnInsert.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);

        db = new DBHandler(getApplicationContext());
    }

    public void buttonAction(View view) {
        switch (view.getId()) {
            case R.id.insert:
                db.insertRecord(edtName.getText().toString());
                Toast.makeText(getApplicationContext(), "record inserted",
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.view:
                tvText.setText(db.getRecords());
                break;
            case R.id.update:
                db.updateRecord(edtId.getText().toString(),
                        edtName.getText().toString());
                Toast.makeText(getApplicationContext(), "record update",
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.delete:
                db.deleteRecord(edtId.getText().toString());
                Toast.makeText(getApplicationContext(), "record delete",
                        Toast.LENGTH_SHORT).show();
                break;
        }

    }

    @Override
    public void onClick(View v) {
        buttonAction(v);
    }
}
