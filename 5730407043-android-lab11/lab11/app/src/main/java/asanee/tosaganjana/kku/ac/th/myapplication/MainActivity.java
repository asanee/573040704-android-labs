package asanee.tosaganjana.kku.ac.th.myapplication;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.provider.DocumentsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class MainActivity extends ListActivity {

    TextView selection;
    ArrayList<String> items = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        selection = (TextView) findViewById(R.id.selection);
        try {
            InputStream in = getResources().openRawResource(R.raw.apps);
            DocumentBuilder builder =
                    DocumentBuilderFactory.newInstance()
                            .newDocumentBuilder();
            Document doc = builder.parse(in, null);
            NodeList apps = doc.getElementsByTagName("app");
            for (int i = 0; i < apps.getLength(); i++) {
                items.add((((Element) apps.item(i)).getAttribute("name")));

            }
        } catch (Exception e) {
            Toast.makeText(this, "Exception " + e.toString(),
                    Toast.LENGTH_LONG).show();
        }
        setListAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, items));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.mainActivity) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.readWriteActivity) {
            Intent intent = new Intent(this, ReadWriteActivity.class);
            startActivity(intent);
            return true;

        }else if (id == R.id.webAPICallerActivity) {
            Intent intent = new Intent(this, WebAPICallerActivity.class);
            startActivity(intent);
            return true;
        }else if (id == R.id.webAPICallerJSONActivity) {
            Intent intent = new Intent(this, WebAPICallerJSONActivity.class);
            startActivity(intent);
            return true;
        }else if (id == R.id.webAPICallerJSONAddressActivity) {
            Intent intent = new Intent(this, WebAPICallerJSONAddressActivity.class);
            startActivity(intent);
            return true;
        }

            return super.onOptionsItemSelected(item);
        }

    public void onListItemClick(ListView parent, View v, int position, long id) {
        selection.setText(items.get(position).toString());
    }
}