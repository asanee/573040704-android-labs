package asanee.tosaganjana.kku.ac.th.myapplication;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class WebAPICallerJSONAddressActivity extends AppCompatActivity {

    EditText emailText;
    ProgressBar progressBar;
    TextView responseView;
    String urlText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_apicaller);

        emailText = (EditText) findViewById(R.id.emailText);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        responseView = (TextView) findViewById(R.id.responseView);

        Button queryButton = (Button) findViewById(R.id.queryButton);
        queryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                urlText = emailText.getText().toString();
                new RetrieveFeedTask().execute();
            }
        });
        getSupportActionBar().setTitle("WebAPICallerJSON Address");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.mainActivity) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.readWriteActivity) {
            Intent intent = new Intent(this, ReadWriteActivity.class);
            startActivity(intent);
            return true;

        }else if (id == R.id.webAPICallerActivity) {
            Intent intent = new Intent(this, WebAPICallerActivity.class);
            startActivity(intent);
            return true;
        }else if (id == R.id.webAPICallerJSONActivity) {
            Intent intent = new Intent(this, WebAPICallerJSONActivity.class);
            startActivity(intent);
            return true;
        }else if (id == R.id.webAPICallerJSONAddressActivity) {
            Intent intent = new Intent(this, WebAPICallerJSONAddressActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    class RetrieveFeedTask extends AsyncTask<Void, Void, String> {

        private Exception exception;

        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            responseView.setText("");
        }

        protected String doInBackground(Void... urls) {

            // Do some validation here

            try {
                String address = URLEncoder.encode(urlText, "UTF-8");
                URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json?address=" + address
                        + "&key=AIzaSyAfVwFwLo9KRXvDpMqeniASHBikFDtEksk");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {
            if (response == null) {
                response = "THERE WAS AN ERROR";
            }
            progressBar.setVisibility(View.GONE);
            Log.i("INFO", response);
            responseView.setText(response);
            // TODO: check this.exception
            // TODO: do something with the feed

            try {
                JSONObject object = (JSONObject) new JSONTokener(response).nextValue();
                JSONArray results = (JSONArray) object.get("results");
                JSONObject resultsGetValue = (JSONObject) results.get(0);
                JSONObject geometry = (JSONObject) resultsGetValue.get("geometry");
                JSONObject location = (JSONObject) geometry.get("location");
                Double lat = (Double) location.get("lat");
                Double lng = (Double) location.get("lng");
                responseView.setText("latitude: " + lat +
                        "\nlongitude: " + lng);


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}

