package asanee.tosaganjana.kku.ac.th.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ReadWriteActivity extends Activity {

    private EditText editText;
    static final int READ_BLOCK_SIZE = 100;
    public static final String TAG = MainActivity.class.getSimpleName();
    String filename = "myFile.txt", fileInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_write);
        initInstances();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.mainActivity) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.readWriteActivity) {
            Intent intent = new Intent(this, ReadWriteActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.webAPICallerActivity) {
            Intent intent = new Intent(this, WebAPICallerActivity.class);
            startActivity(intent);
            return true;
        }else if (id == R.id.webAPICallerJSONActivity) {
            Intent intent = new Intent(this, WebAPICallerJSONActivity.class);
            startActivity(intent);
            return true;
        }else if (id == R.id.webAPICallerJSONAddressActivity) {
            Intent intent = new Intent(this, WebAPICallerJSONAddressActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    protected void initInstances(){

        editText = (EditText) findViewById(R.id.edit_data);
        editText.setSelection(0);
        fileInfo = getFileStreamPath(filename).getAbsolutePath();

    }

    public void writeFile(View v){
        try{
            FileOutputStream fileOut = openFileOutput(filename, MODE_PRIVATE);
            OutputStreamWriter outputWriter = new OutputStreamWriter(fileOut);
            String data = editText.getText().toString();
            outputWriter.write(data);
            outputWriter.close();
            String msg = "write " + data + " to file " + fileInfo;
            Log.d(TAG, msg);
            Toast.makeText(ReadWriteActivity.this, msg, Toast.LENGTH_LONG).show();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    public void readFile(View v){
        String line;
        StringBuffer output = new StringBuffer();
        try{
            FileInputStream fileIn = openFileInput(filename);
            InputStreamReader inputReader = new InputStreamReader(fileIn);
            BufferedReader bufferReader = new BufferedReader(inputReader);
            while ((line = bufferReader.readLine()) != null) {
                output.append(line);
                Log.d(TAG, "READ " + line);
            }
            inputReader.close();
            bufferReader.close();
            fileIn.close();

            editText.setText(output);
            String msg = "read " + output + " form file " +fileInfo;
            Log.d(TAG, msg);
            Toast.makeText(ReadWriteActivity.this, msg, Toast.LENGTH_LONG).show();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}