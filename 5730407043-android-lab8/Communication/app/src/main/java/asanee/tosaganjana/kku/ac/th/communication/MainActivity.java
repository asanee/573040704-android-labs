package asanee.tosaganjana.kku.ac.th.communication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private static final int REQUEST_CODE = 10;

    TextView txResult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Main Activity");

        Button btn = (Button) findViewById(R.id.button);
        txResult = (TextView) findViewById(R.id.textResult);

        btn.setOnClickListener(this);
//        okButtonClicked(btn);
    }


    public void okButtonClicked(View view) {
        EditText name = (EditText) findViewById(R.id.editText);
        // To do 1. create new intent
        Intent intent = new Intent(this,ResultActivity.class);
        intent.putExtra("Name",name.getText().toString());
        // To do 2. use startActivityForResult
        //  with REQUEST_CODE
        startActivityForResult(intent , REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check that it is the SecondActivity with an OK result
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                // get String data from Intent
                String returnString = data.getStringExtra("keyName");

                // set text view with string
                TextView textView = (TextView) findViewById(R.id.textResult);
                textView.setText(returnString);
            }
        }
    }

    @Override
    public void onClick(View v) {
        okButtonClicked(v);
    }
}



