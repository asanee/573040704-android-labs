package asanee.tosaganjana.kku.ac.th.communication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        setTitle("Result Activity");

        String data = getIntent().getExtras().getString("Name");

        TextView result =(TextView) findViewById(R.id.textView2);

        result.setText(data);
    }

    @Override
    public void finish() {
        EditText editText = (EditText) findViewById(R.id.editText2);
        String stringToPassBack = editText.getText().toString();

        // put the String to pass back into an Intent and close this activity
        Intent intent = new Intent();
        intent.putExtra("keyName", stringToPassBack);
        setResult(RESULT_OK, intent);
        super.finish();
    }

}
