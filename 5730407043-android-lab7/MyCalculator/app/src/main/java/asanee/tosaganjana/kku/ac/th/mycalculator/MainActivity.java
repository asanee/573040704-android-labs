package asanee.tosaganjana.kku.ac.th.mycalculator;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements
        View.OnClickListener, CompoundButton.OnCheckedChangeListener, RadioGroup.OnCheckedChangeListener {

    EditText editText_1, editText_2;
    TextView text;
    Button btn_cal;
    RadioButton radioAdd, radioSub, radioMulti, radioDivide;
    RadioGroup operator;
    Double result = 0.0;
    int[] input;
    Double[] val;
    Switch sw;
    Boolean flag;
    Menu menuSetting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        editText_1 = (EditText) findViewById(R.id.editText1);
        editText_2 = (EditText) findViewById(R.id.editText2);

        text = (TextView) findViewById(R.id.textView);

        btn_cal = (Button) findViewById(R.id.btn_cal);
        btn_cal.setOnClickListener(this);

        radioAdd = (RadioButton) findViewById(R.id.radioAdd);
        radioSub = (RadioButton) findViewById(R.id.radioSub);
        radioMulti = (RadioButton) findViewById(R.id.radioMultiply);
        radioDivide = (RadioButton) findViewById(R.id.radioDivide);

        radioAdd.setOnClickListener(this);
        radioSub.setOnClickListener(this);
        radioMulti.setOnClickListener(this);
        radioDivide.setOnClickListener(this);

        operator = (RadioGroup) findViewById(R.id.operator);
        operator.setOnCheckedChangeListener(this);

        sw = (Switch) findViewById(R.id.switch1);
        sw.setChecked(false);
        sw.setOnCheckedChangeListener(this);

        menuSetting = (Menu) findViewById(R.id.action_settings);

        //getWeithAndHeight(MainActivity);

        DisplayMetrics dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        Toast.makeText(this, "width = " + width + " height = " + height, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onClick(View v) {

        if (v == btn_cal) {

            calculate(operator.getCheckedRadioButtonId());

            Intent intent = new Intent(this, SecondActivity.class);
            intent.putExtra("Result",String.valueOf(result));
            startActivity(intent);
        }

    }


    private void calculate(int id) {


        acceptNumbers();

        long startTime = System.currentTimeMillis();

        if (flag && sw.isChecked()) {
            switch (id) {
                case R.id.radioAdd:
                    result = val[0] + val[1];
                    break;
                case R.id.radioSub:

                    result = val[0] - val[1];
                    break;
                case R.id.radioMultiply:

                    result = val[0] * val[1];
                    break;
                case R.id.radioDivide:

                    if (val[1] == 0) {
                        result = 0.0;
                        showToast("Please divide by a non-zero number");
                    } else {
                        result = val[0] / val[1];
                    }

                    break;
            }

            text.setText(" = " + result);



            long endTime = System.currentTimeMillis() - startTime;
            double Time = endTime / 1000.0;
            Log.d("Calculate", String.valueOf(Time));
        }

    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        //check the current state before we display the screen
        if (sw.isChecked()) {
            sw.setText("ON");
            calculate(operator.getCheckedRadioButtonId());
        } else {
            sw.setText("OFF");

        }
    }

    //Show Toast
    private void showToast(String msg) {

        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
        toast.show();

    }


    private void acceptNumbers() {

        long startTime = System.currentTimeMillis();

        try {

            input = new int[2];
            val = new Double[2];

            input[0] = Integer.parseInt(editText_1.getText().toString());
            input[1] = Integer.parseInt(editText_2.getText().toString());


            val[0] = Double.valueOf(input[0]);
            val[1] = Double.valueOf(input[1]);

            flag = true;
            //Log.d("test",);
        } catch (Exception e) {
            flag = false;
            showToast("Please only a number");
        }


        long endTime = System.currentTimeMillis() - startTime;
        double Time = endTime / 1000.0;
        Log.d("Calculate accept input", String.valueOf(Time));

    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        calculate(checkedId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_settings) {
            Toast toast = Toast.makeText(this, "Choose action settings", Toast.LENGTH_LONG);
            toast.show();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
