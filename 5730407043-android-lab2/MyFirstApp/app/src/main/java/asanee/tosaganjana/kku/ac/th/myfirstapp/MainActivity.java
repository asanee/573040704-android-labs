package asanee.tosaganjana.kku.ac.th.myfirstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button btn_Send;
    EditText someText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        btn_Send = (Button) findViewById(R.id.btnSend);
        someText = (EditText) findViewById(R.id.editText);

        btn_Send.setOnClickListener(this);
        someText.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnSend){
            Log.d("tag", String.valueOf(someText.getText()));

            Intent intent = new Intent(this, MassageActivity.class);
            intent.putExtra("Text",String.valueOf(someText.getText()));

            startActivity(intent);
        }
    }
}
