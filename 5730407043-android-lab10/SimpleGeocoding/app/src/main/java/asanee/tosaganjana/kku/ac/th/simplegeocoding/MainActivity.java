package asanee.tosaganjana.kku.ac.th.simplegeocoding;


import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    EditText etLat, etLng, etAddress;
    TextView tvResult;
    Button btnFetch;
    RadioButton rbLatLng;
    RadioButton rbAddress;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

//        //noinspection SimplifiableIfStatement
        if (id == R.id.simple_geocoder) {
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);

            return true;
        } else if(id == R.id.current_location){
            Intent intent = new Intent(this,CurrentLocationActivity.class);
            startActivity(intent);

            return true;
        }else if(id == R.id.current_geocoder){
            Intent intent = new Intent(this,CurrentGeocoderActivity.class);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etLat = (EditText) findViewById(R.id.lat);
        etLng = (EditText) findViewById(R.id.lng);
        etAddress = (EditText) findViewById(R.id.address);
        tvResult = (TextView) findViewById(R.id.answer);
        btnFetch = (Button) findViewById(R.id.btnFetch);
        rbLatLng = (RadioButton) findViewById(R.id.radio_lat_lng);
        rbAddress = (RadioButton) findViewById(R.id.radio_address);


        btnFetch.setOnClickListener(this);
        rbAddress.setOnClickListener(this);
        rbLatLng.setOnClickListener(this);

    }

    private void checkFunction() {
        if (rbLatLng.isChecked()) {

            etLat.setEnabled(true);
            etLng.setEnabled(true);
            etAddress.setEnabled(false);

            etLat.requestFocus();
        } else if (rbAddress.isChecked()) {

            etLat.setEnabled(false);
            etLng.setEnabled(false);
            etAddress.setEnabled(true);

            etAddress.requestFocus();
        }
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                android.location.Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("My Current ", "" + strReturnedAddress.toString());
            } else {
                Log.w("My Current ", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current ", "Canont get Address!");
        }
        return strAdd;
    }

    public void getLocationFromAddress(String strAddress) {

        Geocoder coder = new Geocoder(this);
        List<Address> address;

        try {
            address = coder.getFromLocationName(strAddress, 5);


            Address location = address.get(0);

            Double lat = location.getLatitude();
            Double lng = location.getLongitude();

            Log.d("======", lat + " " + lng);
            tvResult.setText("Lat:" + lat + " \nLng:" + lng);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {
        if (v == btnFetch) {

            if (rbLatLng.isChecked()) {

                double lat = Double.parseDouble(etLat.getText().toString());
                double lng = Double.parseDouble(etLng.getText().toString());

                String resultAddress = getCompleteAddressString(lat, lng);
                Log.d("===", lat + "  " + lng);
//                    String resultAddress = getCompleteAddressString(16.43, 102.8);

                tvResult.setText(resultAddress);

            } else if (rbAddress.isChecked()) {

                String address = etAddress.getText().toString();
                getLocationFromAddress(address);

            }

        }

        if(v == rbAddress || v == rbLatLng){
            checkFunction();
        }
    }
}
